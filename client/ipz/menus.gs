// (C) 2016 Bogdan Gureev

//------------------------------------------------------------------------------

function createMenu()
{
	var ui = SpreadsheetApp.getUi();
	ui.createMenu( "Teacher Tools" )
		.addItem( "Configuration...", "openConfigurationPage" )
		.addSeparator()
		.addItem( "Refresh", "refresh" )
		.addItem( "Refresh (1st semester)", "refresh1" )
		.addItem( "Refresh (2nd semester)", "refresh2" )
		.addSeparator()
		.addItem( "Setup Students With Marks Count Summary...", "setupStudentsWithMarksCountSummary" )
		.addItem( "Setup Students With Green Marks Count Summary...", "setupStudentsWithGreenMarksCountSummary" )
		.addItem( "Setup Students With Red Marks Count Summary...", "setupStudentsWithRedMarksCountSummary" )
		.addToUi();
}

//------------------------------------------------------------------------------

function createGroupsInfo()
{
	var info = [
			[ "КИ-13-1", [ 2, 24 ] ]
		,	[ "КИ-13-2", [ 2, 24 ] ]
		,	[ "КИ-13-3", [ 2, 21 ] ]
		,	[ "КИ-13-4", [ 2, 20 ] ]
		,	[ "КИ-13-5", [ 2, 23 ] ]
		,	[ "КИ-13-6", [ 2, 24 ] ]
		,	[ "КИу-14-6", [ 2, 20 ] ]
		,	"'Лабы'!G4"
	];
	return info;
}

//------------------------------------------------------------------------------

function openConfigurationPage()
{
	var page = TeacherTools.Html.getConfigurationPage( key() );
	SpreadsheetApp.getUi().showModalDialog( page.body, page.title );
}

//------------------------------------------------------------------------------

function refresh()
{
	refresh1();
	refresh2();
}

//------------------------------------------------------------------------------

function refresh1()
{
	TeacherTools.Updates.toggleValue( "'Лабы'!G3", " " );
}

//------------------------------------------------------------------------------

function refresh2()
{
	TeacherTools.Updates.toggleValue( "'Лабы'!G4", " " );
}

//------------------------------------------------------------------------------

function setupStudentsWithMarksCountSummary()
{
	TeacherTools.Menus.setupFunctionCall(
			createGroupsInfo()
		,	"printStudentsWithMarksCountSummary"
	);
}

//------------------------------------------------------------------------------

function setupStudentsWithGreenMarksCountSummary()
{
	TeacherTools.Menus.setupFunctionCall(
			createGroupsInfo()
		,	"printStudentsWithGreenMarksCountSummary"
	);
}

//------------------------------------------------------------------------------

function setupStudentsWithRedMarksCountSummary()
{
	TeacherTools.Menus.setupFunctionCall(
			createGroupsInfo()
		,	"printStudentsWithRedMarksCountSummary"
	);
}

//------------------------------------------------------------------------------
