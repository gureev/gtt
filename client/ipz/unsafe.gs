// (C) 2016 Bogdan Gureev

function __unsafe_printDebtorStudentsMarksCountSummary()
{
    var _groupsList = Array.prototype.slice.call( arguments );

    var baseColumnIndex = 3;
    var baseRowIndex = 1;
    var result = [];
    
    for ( var i = 0; i < _groupsList.length - 1; ++i )
    {
        var groupInfo = TeacherTools.Extractors.Range.getRange( _groupsList[ i ] ).getDisplayValues();
        for ( var rowIndex = baseRowIndex; rowIndex < groupInfo.length; ++rowIndex )
        {
            var row = groupInfo[ rowIndex ];
            var rowResult = [];
            
            var labsCount = 0;
            var modulesCount = 0;
            for ( var columnIndex = baseColumnIndex; columnIndex < row.length; columnIndex += 4 )
            {
                if ( row[ columnIndex ] )
                    ++labsCount;
            }
            for ( var columnIndex = baseColumnIndex + 2; columnIndex < row.length; columnIndex += 4 )
            {
                if ( row[ columnIndex ] )
                    ++modulesCount;
            }

            if ( labsCount == 4 && modulesCount == 4 )
                continue;
            
            rowResult.push( row[ 0 ] );
            rowResult.push( "" ); // NOTE: Duplicate because Name column wraps 2 columns instead of 1
            rowResult.push( labsCount );
            rowResult.push( modulesCount );

            result.push( rowResult );
        }
    }

    if ( result.length > 0 )
    {
        result.sort(
            function ( _left, _right )
            {
                var leftLabs = parseInt( _left[ 2 ] );
                var rightLabs = parseInt( _right[ 2 ] );
                
                if ( leftLabs === rightLabs )
                {
                    var leftModules = parseInt( _left[ 3 ] );
                    var rightModules = parseInt( _right[ 3 ] );
                    
                    if ( leftModules === rightModules )
                        return 0;

                    return ( leftModules < rightModules ) ? 1 : -1;
                }

                return ( leftLabs < rightLabs ) ? 1 : -1;
            }
        );
    }
    
    return result;
}