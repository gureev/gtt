// (C) 2016 Bogdan Gureev

//------------------------------------------------------------------------------

function printDeadlineLabel( _deadlineDate )
{
	return TeacherTools.Deadlines.getLabel( _deadlineDate );
}

//------------------------------------------------------------------------------

function printDeadlineDays( _todayDate, _deadlineDate )
{
	return TeacherTools.Deadlines.getDays( _todayDate, _deadlineDate );
}

//------------------------------------------------------------------------------
