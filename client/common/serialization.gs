// (C) 2016 Bogdan Gureev

//------------------------------------------------------------------------------

function serializeConfiguration()
{
	return TeacherTools.Resources.serializeConfiguration( key() );
}

//------------------------------------------------------------------------------

function deserializeConfiguration( _values )
{
	TeacherTools.Resources.deserializeConfiguration( key(), _values );
}

//------------------------------------------------------------------------------
