// (C) 2016 Bogdan Gureev

//------------------------------------------------------------------------------

function printStudentsWithMarksCountSummary() // NOTE: Variadic arguments
{
	var groupsList = Array.prototype.slice.call( arguments );
	return TeacherTools.API.getStudentsWithMarksCount( key(), groupsList );
}

//------------------------------------------------------------------------------

function printStudentsWithGreenMarksCountSummary() // NOTE: Variadic arguments
{
	var groupsList = Array.prototype.slice.call( arguments );
	return TeacherTools.API.getStudentsWithGreenMarksCount( key(), groupsList );
}

//------------------------------------------------------------------------------

function printStudentsWithRedMarksCountSummary() // NOTE: Variadic arguments
{
	var groupsList = Array.prototype.slice.call( arguments );
	return TeacherTools.API.getStudentsWithRedMarksCount( key(), groupsList );
}

//------------------------------------------------------------------------------

function printStudentSummaryMark() // NOTE: Variadic arguments
{
	var marksList = Array.prototype.slice.call( arguments );
	return TeacherTools.API.getStudentSummaryMark( key(), marksList );
}

//------------------------------------------------------------------------------

function printStudentIntermediateMark() // NOTE: Variadic arguments
{
	var marksList = Array.prototype.slice.call( arguments );
	return TeacherTools.API.getStudentIntermediateMark( key(), marksList );
}

//------------------------------------------------------------------------------

function printStudentExamMark( _keptMark, _summaryMark, _examMark )
{
	return TeacherTools.API.getStudentExamMark(
			key()
		,	_keptMark
		,	_summaryMark
		,	_examMark
	);
}

//------------------------------------------------------------------------------

function printMarkTypesSummary() // NOTE: Variadic arguments
{
	var groupsList = Array.prototype.slice.call( arguments );
	return TeacherTools.API.getMarkTypes( key(), groupsList );
}

//------------------------------------------------------------------------------

function printStudentsWithThemesCountSummary() // NOTE: Variadic arguments
{
	var groupsList = Array.prototype.slice.call( arguments );
	return TeacherTools.API.getStudentsWithThemesCount( key(), groupsList );
}

//------------------------------------------------------------------------------

function printThemesPerformanceSummary() // NOTE: Variadic arguments
{
	var groupsList = Array.prototype.slice.call( arguments );
	return TeacherTools.API.getThemesPerformance( key(), groupsList );
}

//------------------------------------------------------------------------------

function printFreeThemesSummary( _textRange )
{
	return TeacherTools.API.getFreeThemesCount( key(), _textRange );
}

//------------------------------------------------------------------------------

function printSentThemesSummary( _textRange )
{
	return TeacherTools.API.getSentThemesCount( key(), _textRange );
}

//------------------------------------------------------------------------------

function printBasicVariantsAssignmentsSummary() // NOTE: Variadic arguments
{
	var groupsList = Array.prototype.slice.call( arguments );
	return TeacherTools.API.getBasicVariantsAssignmentTable( key(), groupsList );
}

//------------------------------------------------------------------------------

function printAdvancedVariantsAssignmentsSummary() // NOTE: Variadic arguments
{
	var groupsList = Array.prototype.slice.call( arguments );
	return TeacherTools.API.getAdvancedVariantsAssignmentTable( key(), groupsList );
}

//------------------------------------------------------------------------------

function printAmbitionVariantsAssignmentsSummary() // NOTE: Variadic arguments
{
	var groupsList = Array.prototype.slice.call( arguments );
	return TeacherTools.API.getAmbitionVariantsAssignmentTable( key(), groupsList );
}

//------------------------------------------------------------------------------
