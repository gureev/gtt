// (C) 2016 Bogdan Gureev

//------------------------------------------------------------------------------

function createMenu()
{
	var ui = SpreadsheetApp.getUi();
	ui.createMenu( "Teacher Tools" )
		.addItem( "Configuration...", "openConfigurationPage" )
		.addSeparator()
		.addItem( "Refresh", "refresh" )
		.addSeparator()
		.addItem( "Setup Students With Marks Count Summary...", "setupStudentsWithMarksCountSummary" )
		.addItem( "Setup Students With Green Marks Count Summary...", "setupStudentsWithGreenMarksCountSummary" )
		.addItem( "Setup Students With Red Marks Count Summary...", "setupStudentsWithRedMarksCountSummary" )
		.addToUi();
}

//------------------------------------------------------------------------------

function createGroupsInfo()
{
	var info = [
			[ "КИ-15-1", [ 2, 26 ] ]
		,	[ "КИ-15-2", [ 2, 23 ] ]
		,	[ "КИ-15-3", [ 2, 26 ] ]
		,	[ "КИ-15-4", [ 2, 25 ] ]
		,	[ "КИ-15-5", [ 2, 30 ] ]
		,	"'Лабы'!X1"
	];
	return info;
}

//------------------------------------------------------------------------------

function openConfigurationPage()
{
	var page = TeacherTools.Html.getConfigurationPage( key() );
	SpreadsheetApp.getUi().showModalDialog( page.body, page.title );
}

//------------------------------------------------------------------------------

function refresh()
{
	TeacherTools.Updates.toggleValue( "'Лабы'!X1", " " );
}

//------------------------------------------------------------------------------

function setupStudentsWithMarksCountSummary()
{
	TeacherTools.Menus.setupFunctionCall(
			createGroupsInfo()
		,	"printStudentsWithMarksCountSummary"
	);
}

//------------------------------------------------------------------------------

function setupStudentsWithGreenMarksCountSummary()
{
	TeacherTools.Menus.setupFunctionCall(
			createGroupsInfo()
		,	"printStudentsWithGreenMarksCountSummary"
	);
}

//------------------------------------------------------------------------------

function setupStudentsWithRedMarksCountSummary()
{
	TeacherTools.Menus.setupFunctionCall(
			createGroupsInfo()
		,	"printStudentsWithRedMarksCountSummary"
	);
}

//------------------------------------------------------------------------------
