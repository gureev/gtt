// (C) 2016 Bogdan Gureev

//------------------------------------------------------------------------------

Menus = {};
(function(){

//------------------------------------------------------------------------------

	function dummy()
	{
		var ui = SpreadsheetApp.getUi();
		ui.alert( Resources.Strings.FeatureHasNotBeenImplemented );
	}

//------------------------------------------------------------------------------

	function setupFunctionCall( _groupsInfos, _function )
	{
		var range = SpreadsheetApp.getActiveRange();
		if ( !Menus.Helpers.isSingleCell( range ) )
			return;

		var columnName = Menus.Helpers.promptColumnName();
		if ( !columnName )
			return;

		var functionCall = Menus.Helpers.generateFunctionCall(
				_groupsInfos
			,	columnName
			,	_function
		);
		range.setValue( functionCall );
	}

//------------------------------------------------------------------------------

	this.dummy = dummy;
	this.setupFunctionCall = setupFunctionCall;

//------------------------------------------------------------------------------

}).apply( Menus );

//------------------------------------------------------------------------------

Menus.Helpers = {};
(function(){

//------------------------------------------------------------------------------

	function getGroupLink( _groupName, _columnName, _rangeBounds )
	{
		var link =
				"\"'"
			+	_groupName
			+	"'!"
			+	_columnName
			+	_rangeBounds[ 0 ]
			+	":"
			+	_columnName
			+	_rangeBounds[ 1 ]
			+	"\";";

		return link;
	}

//------------------------------------------------------------------------------

	function generateFunctionCall( _groupsInfos, _columnName, _function )
	{
		var groupsList = " ";

		var groupsCount = _groupsInfos.length;
		for ( var i = 0; i < groupsCount; ++i )
		{
			var groupInfo = _groupsInfos[ i ];
			if ( typeof groupInfo != "object" )
				groupsList += groupInfo;
			else
				groupsList += getGroupLink(
						groupInfo[ 0 ]
					,	_columnName
					,	groupInfo[ 1 ]
				);

			groupsList += " ";
		}

		return "=" + _function + "(" + groupsList + ")";
	}

//------------------------------------------------------------------------------

	function isSingleCell( _range )
	{
		if ( _range.getHeight() != 1 || _range.getWidth() != 1 )
		{
			var ui = SpreadsheetApp.getUi();
			ui.alert( Resources.Strings.OnlyOneCellMustBeSelected );

			return false;
		}

		return true;
	}

//------------------------------------------------------------------------------

	function promptColumnName()
	{
		var ui = SpreadsheetApp.getUi();
		var result = ui.prompt(
				Resources.Strings.EnterColumnNameTitle
			,	Resources.Strings.EnterColumnNameText
			,	ui.ButtonSet.OK_CANCEL
		);

		var button = result.getSelectedButton();
		if ( button == ui.Button.CANCEL )
			return;

		return result.getResponseText();
	}

//------------------------------------------------------------------------------

	this.generateFunctionCall = generateFunctionCall;
	this.isSingleCell = isSingleCell;
	this.promptColumnName = promptColumnName;

//------------------------------------------------------------------------------

}).apply( Menus.Helpers );

//------------------------------------------------------------------------------
