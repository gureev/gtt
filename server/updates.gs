// (C) 2016 Bogdan Gureev

//------------------------------------------------------------------------------

Updates = {};
(function(){

//------------------------------------------------------------------------------

	function makeBackup()
	{
		var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
		spreadsheet.copy( spreadsheet.getName() );
	}

//------------------------------------------------------------------------------

	function toggleValue( _textRange, _value )
	{
		var range = Extractors.Range.getRange( _textRange );

		var value = range.getValue();
		if ( value == _value )
			range.clearContent();
		else
			range.setValue( _value );
	}

//------------------------------------------------------------------------------

	this.makeBackup = makeBackup;
	this.toggleValue = toggleValue;

//------------------------------------------------------------------------------

}).apply( Updates );

//------------------------------------------------------------------------------
