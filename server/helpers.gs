// (C) 2016 Bogdan Gureev

//------------------------------------------------------------------------------

Helpers = {};
(function(){

//------------------------------------------------------------------------------

	function getPercentagesValue( _value, _full )
	{
		var ratio = _value / _full * 100;
		return Math.round( ratio );
	}

//------------------------------------------------------------------------------

	function isEmpty( _string )
	{
		if ( typeof _string != "string" )
			return false;

		if ( /\S/.test( _string ) )
			return false;

		return true;
	}

//------------------------------------------------------------------------------

	function isInt( _value )
	{
		return Extractors.getIntValue( _value ) === _value;
	}

//------------------------------------------------------------------------------

	function isDouble( _value )
	{
		return Extractors.getDoubleValue( _value ) === _value;
	}

//------------------------------------------------------------------------------

	function assignIntValue( _array, _value )
	{
		if ( _array === undefined )
			throw "assignIntValue: _array is undefined";

		var value = Extractors.getSafeIntValue( _value );
		_array.push( value );
	}

//------------------------------------------------------------------------------

	function assignArrayValue( _array, _value )
	{
		if ( _array === undefined )
			throw "assignArrayValue: _array is undefined";

		Array.prototype.push.apply( _array, _value );
	}

//------------------------------------------------------------------------------

	this.getPercentagesValue = getPercentagesValue;
	this.isEmpty = isEmpty;
	this.isInt = isInt;
	this.isDouble = isDouble;

	this.assignIntValue = assignIntValue;
	this.assignArrayValue = assignArrayValue;

//------------------------------------------------------------------------------

}).apply( Helpers );

//------------------------------------------------------------------------------
