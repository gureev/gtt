// (C) 2016 Bogdan Gureev

//------------------------------------------------------------------------------

Cache = {};
(function(){

//------------------------------------------------------------------------------

	var keys = [
			"ipz.ki13"
		,	"ipz.ki14"

		,	"oop.ki15"

		,	"sta.ki16"
	];

//------------------------------------------------------------------------------

	function validateKey( _key )
	{
		if ( Helpers.isEmpty( _key ) )
			throw "validateKey: key is empty";

		var result = false;

		var keysCount = keys.length;
		for ( var i = 0; i < keysCount; ++i )
			if ( _key.indexOf( keys[ i ] ) == 0 )
			{
				result = true;
				break;
			}

		if ( !result )
			throw "validateKey: key is not registered";
	}

//------------------------------------------------------------------------------

	function getCache()
	{
		return CacheService.getScriptCache();
	}

//------------------------------------------------------------------------------

	function set( _key, _value )
	{
		validateKey( _key );
		getCache().put( _key, _value );
	}

//------------------------------------------------------------------------------

	function get( _key )
	{
		validateKey( _key );
		return getCache().get( _key );
	}

//------------------------------------------------------------------------------

	function remove( _key )
	{
		validateKey( _key );
		getCache().remove( _key );
	}

//------------------------------------------------------------------------------

	function createKey( _primaryKey, _resourceKey )
	{
		return _primaryKey + "." + _resourceKey;
	}

//------------------------------------------------------------------------------

	this.get = get;
	this.set = set;
	this.remove = remove;
	this.createKey = createKey;

//------------------------------------------------------------------------------

}).apply( Cache );

//------------------------------------------------------------------------------
