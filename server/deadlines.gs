// (C) 2016 Bogdan Gureev

//------------------------------------------------------------------------------

Deadlines = {};
(function(){

//------------------------------------------------------------------------------

	function getDaysDependentName( _days, _dependentNames )
	{
		var name = "";

		var sliceDay = Extractors.getSafeIntValue( String( _days ).slice( -1 ) );
		var daysCount = Extractors.getSafeIntValue( _days );

		if (
				sliceDay == 1
			&&	( daysCount < 10 || daysCount > 20 )
		)
		{
			name = _dependentNames[ 0 ];
		}
		else if (
				( sliceDay == 2 || sliceDay == 3 || sliceDay == 4 )
			&&	( daysCount < 10 || daysCount > 20 )
		)
		{
			name = _dependentNames[ 1 ];
		}
		else
		{
			name = _dependentNames[ 2 ];
		}

		return name;
	}

//------------------------------------------------------------------------------

	function getDays( _todayDate, _deadlineDate )
	{
		var todayDate = new Date( _todayDate );
		var deadlineDate = new Date( _deadlineDate );

		var timeDiff = deadlineDate.getTime() - todayDate.getTime();
		var daysDiff = Math.ceil( timeDiff / ( 1000 * 3600 * 24 ) );

		if ( timeDiff < 0 )
			return Resources.Strings.DeadlineIsOver;
		if ( daysDiff == 0 )
			return Resources.Strings.DeadlineIsToday;

		var prefix = getDaysDependentName( daysDiff, Resources.Strings.DeadlineDaysPrefix );
		var suffix = getDaysDependentName( daysDiff, Resources.Strings.DeadlineDaysSuffix );
		return prefix + " " + daysDiff + " " + suffix + "!";
	}

//------------------------------------------------------------------------------

	function getLabel( _deadlineDate )
	{
		var label = Resources.Strings.DeadlineLabel;

		if ( _deadlineDate )
			return label + _deadlineDate;

		return label + Resources.Strings.DeadlineUnknown;
	}

//------------------------------------------------------------------------------

	this.getLabel = getLabel;
	this.getDays = getDays;

//------------------------------------------------------------------------------

}).apply( Deadlines );

//------------------------------------------------------------------------------
