// (C) 2016 Bogdan Gureev

//------------------------------------------------------------------------------

Properties = {};
(function(){

//------------------------------------------------------------------------------

	var keys = [
			"ipz.ki13"
		,	"ipz.ki14"

		,	"oop.ki15"

		,	"sta.ki16"
	];

//------------------------------------------------------------------------------

	function validateKey( _key )
	{
		if ( Helpers.isEmpty( _key ) )
			throw "validateKey: key is empty";

		var result = false;

		var keysCount = keys.length;
		for ( var i = 0; i < keysCount; ++i )
			if ( _key.indexOf( keys[ i ] ) == 0 )
			{
				result = true;
				break;
			}

		if ( !result )
			throw "validateKey: key is not registered";
	}

//------------------------------------------------------------------------------

	function getProperties()
	{
		return PropertiesService.getScriptProperties();
	}

//------------------------------------------------------------------------------

	function set( _key, _value )
	{
		validateKey( _key );
		getProperties().setProperty( _key, _value );
	}

//------------------------------------------------------------------------------

	function get( _key )
	{
		validateKey( _key );
		return getProperties().getProperty( _key );
	}

//------------------------------------------------------------------------------

	function remove( _key )
	{
		validateKey( _key );
		getProperties().deleteProperty( _key );
	}

//------------------------------------------------------------------------------

	function createKey( _primaryKey, _resourceKey )
	{
		return _primaryKey + "." + _resourceKey;
	}

//------------------------------------------------------------------------------

	this.get = get;
	this.set = set;
	this.remove = remove;
	this.createKey = createKey;

//------------------------------------------------------------------------------

}).apply( Properties );

//------------------------------------------------------------------------------
