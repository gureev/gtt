// (C) 2016 Bogdan Gureev

//------------------------------------------------------------------------------

API = {};
(function(){

//------------------------------------------------------------------------------

	function adjustArguments( _arguments )
	{
		// NOTE: Assume last argument is Updater
		if ( _arguments.length > 0 )
			_arguments.pop();
	}

//------------------------------------------------------------------------------

	function getStudentsWithMarksCount( _key, _groupsList )
	{
		Resources.ensureLoaded( _key );
		return Marks.getStudentsWithMarksCount( _groupsList );
	}

//------------------------------------------------------------------------------

	function getStudentsWithRedMarksCount( _key, _groupsList )
	{
		adjustArguments( _groupsList );
		Resources.ensureLoaded( _key );
		return Marks.getStudentsWithRedMarksCount( _groupsList );
	}

//------------------------------------------------------------------------------

	function getStudentsWithGreenMarksCount( _key, _groupsList )
	{
		adjustArguments( _groupsList );
		Resources.ensureLoaded( _key );
		return Marks.getStudentsWithGreenMarksCount( _groupsList );
	}

//------------------------------------------------------------------------------

	function getStudentSummaryMark( _key, _marksList )
	{
		Resources.ensureLoaded( _key );
		return Marks.getStudentSummaryMark( _marksList );
	}

//------------------------------------------------------------------------------

	function getStudentIntermediateMark( _key, _marksList )
	{
		Resources.ensureLoaded( _key );
		return Marks.getStudentIntermediateMark( _marksList );
	}

//------------------------------------------------------------------------------

	function getStudentExamMark( _key, _keptMark, _summaryMark, _examMark )
	{
		Resources.ensureLoaded( _key );
		return Marks.getStudentExamMark( _keptMark, _summaryMark, _examMark );
	}

//------------------------------------------------------------------------------

	function getMarkTypes( _key, _groupsList )
	{
		Resources.ensureLoaded( _key );
		return Marks.getMarkTypes( _groupsList );
	}

//------------------------------------------------------------------------------

	function getBasicVariantsAssignmentTable( _key, _groupsList )
	{
		Resources.ensureLoaded( _key );
		return Variants.getBasicAssignmentTable( _groupsList );
	}

//------------------------------------------------------------------------------

	function getAdvancedVariantsAssignmentTable( _key, _groupsList )
	{
		Resources.ensureLoaded( _key );
		return Variants.getAdvancedAssignmentTable( _groupsList );
	}

//------------------------------------------------------------------------------

	function getAmbitionVariantsAssignmentTable( _key, _groupsList )
	{
		Resources.ensureLoaded( _key );
		return Variants.getAmbitionAssignmentTable( _groupsList );
	}

//------------------------------------------------------------------------------

	function getStudentsWithThemesCount( _key, _groupsList )
	{
		adjustArguments( _groupsList );
		Resources.ensureLoaded( _key );
		return Themes.getStudentsWithThemesCount( _groupsList );
	}

//------------------------------------------------------------------------------

	function getFreeThemesCount( _key, _textRange )
	{
		Resources.ensureLoaded( _key );
		return Themes.getFreeThemesCount( _textRange );
	}

//------------------------------------------------------------------------------

	function getSentThemesCount( _key, _textRange )
	{
		Resources.ensureLoaded( _key );
		return Themes.getSentThemesCount( _textRange );
	}

//------------------------------------------------------------------------------

	function getThemesPerformance( _key, _groupsList )
	{
		Resources.ensureLoaded( _key );
		return Themes.getThemesPerformance( _groupsList );
	}

//------------------------------------------------------------------------------

	this.getStudentsWithMarksCount = getStudentsWithMarksCount;
	this.getStudentsWithRedMarksCount = getStudentsWithRedMarksCount;
	this.getStudentsWithGreenMarksCount = getStudentsWithGreenMarksCount;
	this.getStudentSummaryMark = getStudentSummaryMark;
	this.getStudentIntermediateMark = getStudentIntermediateMark;
	this.getStudentExamMark = getStudentExamMark;
	this.getMarkTypes = getMarkTypes;

	this.getBasicVariantsAssignmentTable = getBasicVariantsAssignmentTable;
	this.getAdvancedVariantsAssignmentTable = getAdvancedVariantsAssignmentTable;
	this.getAmbitionVariantsAssignmentTable = getAmbitionVariantsAssignmentTable;

	this.getStudentsWithThemesCount = getStudentsWithThemesCount;
	this.getFreeThemesCount = getFreeThemesCount;
	this.getSentThemesCount = getSentThemesCount;
	this.getThemesPerformance = getThemesPerformance;

//------------------------------------------------------------------------------

}).apply( API );

//------------------------------------------------------------------------------
