// (C) 2016 Bogdan Gureev

//------------------------------------------------------------------------------

Html = {};
(function(){

//------------------------------------------------------------------------------

	function getConfigurationPage( _key )
	{
		var template = HtmlService.createTemplateFromFile( "configuration" );
		//template.key = _key; // NOTE: Key is not longer used in template

		var output = template.evaluate();
		output.setTitle( "Configuration" );
		output.setWidth( 90 + 400 ); // NOTE: Google Dialog Border + Form Width
		output.setHeight( 110 + 500 ); // NOTE: Google Dialog Border + Form Height

		return {
				title: "Configuration"
			,	body: output
		};
	}

//------------------------------------------------------------------------------

	this.getConfigurationPage = getConfigurationPage;

//------------------------------------------------------------------------------

}).apply( Html );

//------------------------------------------------------------------------------
