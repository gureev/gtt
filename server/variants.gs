// (C) 2016 Bogdan Gureev

//------------------------------------------------------------------------------

Variants = {};
(function(){

//------------------------------------------------------------------------------

	function getAssignmentTable( _groupsList, _variantPrefix )
	{
		var groupsCount = _groupsList.length;

		var table = [];
		var variantRegExp = new RegExp( _variantPrefix + "\\d+", "g" );

		for ( var i = 0; i < groupsCount; ++i )
		{
			var groupData = _groupsList[ i ];

			if ( Helpers.isEmpty( groupData ) )
				continue;

			Variants.Handlers.getAssignmentTable(
					table
				,	groupData
				,	variantRegExp
				,	_variantPrefix
			);
		}

		if ( table.length > 0 )
		{
			table.shift();
			return table;
		}

		return [ 0 ];
	}

//------------------------------------------------------------------------------

	function getBasicAssignmentTable( _groupsList )
	{
		return getAssignmentTable(
				_groupsList
			,	Resources.Configuration.VariantBasic
		);
	}

//------------------------------------------------------------------------------

	function getAdvancedAssignmentTable( _groupsList )
	{
		return getAssignmentTable(
				_groupsList
			,	Resources.Configuration.VariantAdvanced
		);
	}

//------------------------------------------------------------------------------

	function getAmbitionAssignmentTable( _groupsList )
	{
		return getAssignmentTable(
				_groupsList
			,	Resources.Configuration.VariantAmbition
		);
	}

//------------------------------------------------------------------------------

	this.getBasicAssignmentTable = getBasicAssignmentTable;
	this.getAdvancedAssignmentTable = getAdvancedAssignmentTable;
	this.getAmbitionAssignmentTable = getAmbitionAssignmentTable;

//------------------------------------------------------------------------------

}).apply( Variants );

//------------------------------------------------------------------------------

Variants.Handlers = {};
(function(){

//------------------------------------------------------------------------------

	function getAssignmentTable( _table, _groupData, _matcher, _variantPrefix )
	{
		Extractors.Array.forEachItem(
				_groupData
			,	function ( _value )
				{
					var variants = _value.match( _matcher );
					if ( !variants )
						return;

					var variantsCount = variants.length;
					for ( var i = 0; i < variantsCount; ++i )
					{
						var variant = Extractors.getSafeIntValue( variants[ i ].replace( _variantPrefix, "" ) );
						if ( !variant )
							continue;

						var count  = Extractors.getSafeIntValue( _table[ variant ] );
						_table[ variant ] = count + 1;
					}
				}
		);
	}

//------------------------------------------------------------------------------

	this.getAssignmentTable = getAssignmentTable;

//------------------------------------------------------------------------------

}).apply( Variants.Handlers );

//------------------------------------------------------------------------------
