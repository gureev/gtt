// (C) 2016 Bogdan Gureev

//------------------------------------------------------------------------------

Marks = {};
(function(){

//------------------------------------------------------------------------------

	function getStudentMark( _marksList, _handler )
	{
		var marksCount = _marksList.length;
		var totalMark = 0;

		for ( var i = 0; i < marksCount; ++i )
		{
			var mark = _marksList[ i ];
			if ( !_handler( mark ) )
				return Resources.Configuration.MarkEmpty;

			totalMark += Extractors.getSafeDoubleValue( mark );
		}

		totalMark = Math.round( totalMark );
		if ( totalMark < 0 || totalMark > 100 )
			return totalMark + Resources.Configuration.MarkInvalid;

		return totalMark;
	}

//------------------------------------------------------------------------------

	function getStudentsWithMarksCount( _groupsList )
	{
		var summary = Extractors.Array.getStudentsCount(
				Marks.Handlers.getStudentsWithMarksCount
			,	_groupsList
		);
		return summary.selected + " / " + summary.all + " ( " + summary.ratio + "% )";
	}

//------------------------------------------------------------------------------

	function getStudentsWithRedMarksCount( _groupsList )
	{
		var summary = Extractors.Range.getStudentsCount(
				Marks.Handlers.getStudentsWithRedMarksCount
			,	_groupsList
		);
		return summary.selected + " ( " + summary.ratio + "% )";
	}

//------------------------------------------------------------------------------

	function getStudentsWithGreenMarksCount( _groupsList )
	{
		var summary = Extractors.Range.getStudentsCount(
				Marks.Handlers.getStudentsWithGreenMarksCount
			,	_groupsList
		);
		return summary.selected + " ( " + summary.ratio + "% )";
	}

//------------------------------------------------------------------------------

	function getStudentSummaryMark( _marksList )
	{
		return getStudentMark(
				_marksList
			,	function ( _mark )
				{
					return Helpers.isDouble( _mark );
				}
		);
	}

//------------------------------------------------------------------------------

	function getStudentIntermediateMark( _marksList )
	{
		return getStudentMark(
				_marksList
			,	function ( _mark )
				{
					return true;
				}
		);
	}

//------------------------------------------------------------------------------

	function getStudentExamMark( _keptMark, _summaryMark, _examMark )
	{
		if (
				Helpers.isEmpty( _keptMark )
			||	Helpers.isEmpty( _summaryMark )
			||	Helpers.isEmpty( _examMark )
		)
			return Resources.Configuration.MarkEmpty;

		var keptMarkMultiplier = Extractors.getDoubleValue( Resources.Configuration.MarkMultiplierKept );
		var summaryMarkMultiplier = Extractors.getDoubleValue( Resources.Configuration.MarkMultiplierSummary );
		var examMarkMultiplier = Extractors.getDoubleValue( Resources.Configuration.MarkMultiplierExam );

		return getStudentSummaryMark(
			[
					_keptMark * keptMarkMultiplier
				,	_summaryMark * summaryMarkMultiplier
				,	_examMark * examMarkMultiplier
			]
		);
	}

//------------------------------------------------------------------------------

	function getMarkTypes( _groupsList )
	{
		var groupsCount = _groupsList.length;

		// NOTE:      A  B  C  D  E  F  FX
		// NOTE:      0  1  2  3  4  5  6
		var types = [ 0, 0, 0, 0, 0, 0, 0 ];

		for ( var i = 0; i < groupsCount; ++i )
		{
			var groupData = _groupsList[ i ];

			if ( Helpers.isEmpty( groupData ) )
				continue;

			Marks.Handlers.getMarkTypes( types, groupData );
		}

		return types;
	}

//------------------------------------------------------------------------------

	this.getStudentsWithMarksCount = getStudentsWithMarksCount;
	this.getStudentsWithRedMarksCount = getStudentsWithRedMarksCount;
	this.getStudentsWithGreenMarksCount = getStudentsWithGreenMarksCount;

	this.getMarkTypes = getMarkTypes;

	this.getStudentSummaryMark = getStudentSummaryMark;
	this.getStudentIntermediateMark = getStudentIntermediateMark;
	this.getStudentExamMark = getStudentExamMark;

//------------------------------------------------------------------------------

}).apply( Marks );

//------------------------------------------------------------------------------

Marks.Handlers = {};
(function(){

//------------------------------------------------------------------------------

	function getStudentsWithMarksCount( _groupData )
	{
		return Extractors.Array.getSpecificItemsCount(
				_groupData
			,	function ( _value )
				{
					return _value;
				}
		);
	}

//------------------------------------------------------------------------------

	function getStudentsWithRedMarksCount( _range )
	{
		var backgrounds = _range.getBackgrounds();

		return Extractors.Range.getSpecificItemsCount(
				backgrounds
			,	function ( _value )
				{
					return _value == Resources.Configuration.ColorRed;
				}
		);
	}

//------------------------------------------------------------------------------

	function getStudentsWithGreenMarksCount( _range )
	{
		var allMarks = Extractors.Range.getSpecificItemsCount(
				_range.getDisplayValues()
			,	function ( _value )
				{
					return _value;
				}
		);

		return allMarks - getStudentsWithRedMarksCount( _range );
	}

//------------------------------------------------------------------------------

	function getMarkTypes( _types, _groupData )
	{
		// NOTE: A  B  C  D  E  F  FX
		// NOTE: 0  1  2  3  4  5  6

		Extractors.Array.forEachItem(
				_groupData
			,	function ( _value )
				{
					var mark = Extractors.getSafeIntValue( _value );
					if ( mark > 100 || mark <= 0 )
						return;

					if ( mark >= 96 )
						++_types[ 0 ];
					else if ( mark >= 90 )
						++_types[ 1 ];
					else if ( mark >= 75 )
						++_types[ 2 ];
					else if ( mark >= 66 )
						++_types[ 3 ];
					else if ( mark >= 60 )
						++_types[ 4 ];
					else if ( mark >= 35 )
						++_types[ 5 ];
					else if ( mark >= 0 )
						++_types[ 6 ];
				}
		);
	}

//------------------------------------------------------------------------------

	this.getStudentsWithMarksCount = getStudentsWithMarksCount;
	this.getStudentsWithRedMarksCount = getStudentsWithRedMarksCount;
	this.getStudentsWithGreenMarksCount = getStudentsWithGreenMarksCount;

	this.getMarkTypes = getMarkTypes;

//------------------------------------------------------------------------------

}).apply( Marks.Handlers );

//------------------------------------------------------------------------------
