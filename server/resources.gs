// (C) 2016 Bogdan Gureev

//------------------------------------------------------------------------------

Resources = {};
(function(){

//------------------------------------------------------------------------------

	var index =
	{
			name:				0
		,	value:				1
		,	description:		2
		,	storage:			3
	};

	var preferences =
	{
			// NOTE: index			0									1							2																			3
			MarkEmpty				:[ ".mark.empty"					, "Нет всех оценок"			, "Label instead of mark when some of important marks are missed"			, true		]
		,	MarkInvalid				:[ ".mark.invalid"					, " (странно)"				, "Suffix for marks which are out-of-range"									, false		]
		,	MarkMultiplierKept		:[ ".mark.multiplier.kept"			, "0.2"						, "Multiplier for first mark in exam mark calculation"						, false		]
		,	MarkMultiplierSummary	:[ ".mark.multiplier.summary"		, "0.6"						, "Multiplier for second mark in exam mark calculation"						, false		]
		,	MarkMultiplierExam		:[ ".mark.multiplier.exam"			, "0.2"						, "Multiplier for third mark in exam mark calculation"						, false		]

		,	VariantBasic			:[ ".variant.basic"					, "Б-"						, "Prefix to detect basic variants"											, false		]
		,	VariantAdvanced			:[ ".variant.advanced"				, "У-"						, "Prefix to detect advanced variants"										, false		]
		,	VariantAmbition			:[ ".variant.ambition"				, "А-"						, "Prefix to detect ambition variants"										, false		]

		,	ColorRed				:[ ".color.red"						, "#f4c7c3"					, "Marks with this background color are treated as after deadline"			, false		]
		,	ColorWhite				:[ ".color.white"					, "#ffffff"					, "Themes without this background color are treated as sent"				, false		]
		,	ColorGrey				:[ ".color.grey"					, "#efefef"					, "Temporary unused"														, false		]

		,	StyleLineThrough		:[ ".style.lineThrough"				, "line-through"			, "Themes with this font style are treated as busy"							, false		]
		,	StyleItalic				:[ ".style.italic"					, "italic"					, "Student's themes without this font style are treated as selected"		, false		]
	};

//------------------------------------------------------------------------------

	function createConfigurationKey( _key )
	{
		return Properties.createKey( _key, "configuration" );
	}

//------------------------------------------------------------------------------

	function saveConfiguration( _key )
	{
		Properties.set( _key, "added" );

		for ( var key in preferences )
		{
			if ( preferences[ key ][ index.storage ] )
			{
				var resourceKey = _key + preferences[ key ][ index.name ];
				Properties.set( resourceKey, Resources.Configuration[ key ] );
			}
		}
	}

//------------------------------------------------------------------------------

	function restoreConfiguration( _key )
	{
		for ( var key in preferences )
		{
			if ( preferences[ key ][ index.storage ] )
			{
				var resourceKey = _key + preferences[ key ][ index.name ];
				Resources.Configuration[ key ] = Properties.get( resourceKey );
			}
			else
			{
				Resources.Configuration[ key ] = preferences[ key ][ index.value ];
			}
		}
	}

//------------------------------------------------------------------------------

	function restoreDefaultConfiguration()
	{
		for ( var key in preferences )
			Resources.Configuration[ key ] = preferences[ key ][ index.value ];
	}

//------------------------------------------------------------------------------

	function restoreConcreteConfiguration( _values )
	{
		for ( var key in preferences )
			Resources.Configuration[ key ] = _values[ key ];
	}

//------------------------------------------------------------------------------

	function ensureLoaded( _key )
	{
		Utilities.sleep( 1000 );

		var key = createConfigurationKey( _key );

		var isSaved = Properties.get( key );
		if ( isSaved )
			restoreConfiguration( key );
		else
			restoreDefaultConfiguration();
	}

//------------------------------------------------------------------------------

	function serializeConfiguration( _key )
	{
		ensureLoaded( _key );

		var actual = preferences;
		for ( var key in preferences )
			actual[ key ][ index.value ] = Resources.Configuration[ key ];

		return actual;
	}

//------------------------------------------------------------------------------

	function deserializeConfiguration( _key, _values )
	{
		var key = createConfigurationKey( _key );

		if ( _values )
			restoreConcreteConfiguration( _values );
		else
			restoreDefaultConfiguration();

		saveConfiguration( key );
	}

//------------------------------------------------------------------------------

	this.ensureLoaded = ensureLoaded;
	this.serializeConfiguration = serializeConfiguration;
	this.deserializeConfiguration = deserializeConfiguration;

//------------------------------------------------------------------------------

}).apply( Resources );

//------------------------------------------------------------------------------

Resources.Configuration = {};
(function(){

//------------------------------------------------------------------------------

	// NOTE: Preferences are generated automatically

//------------------------------------------------------------------------------

}).apply( Resources.Configuration );

//------------------------------------------------------------------------------

Resources.Strings = {};
(function(){

//------------------------------------------------------------------------------

	this.OnlyOneCellMustBeSelected = "Output cell is not selected! Only 1 cell must be selected!";
	this.FeatureHasNotBeenImplemented = "This feature has not been implemented yet!";

//------------------------------------------------------------------------------

	this.EnterColumnNameTitle = "Please confirm the column name";
	this.EnterColumnNameText = "Please enter the column name (E, F, I, etc.):";

//------------------------------------------------------------------------------

	this.DeadlineLabel = "Deadline: ";
	this.DeadlineUnknown = "Не назначен";
	this.DeadlineIsOver = "Deadline прошёл!";
	this.DeadlineIsToday = "Это сегодня!";

//------------------------------------------------------------------------------

	this.DeadlineDaysPrefix = [ "Остался", "Осталось", "Осталось" ];
	this.DeadlineDaysSuffix = [ "день", "дня", "дней" ];

//------------------------------------------------------------------------------

}).apply( Resources.Strings );

//------------------------------------------------------------------------------
