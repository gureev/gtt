// (C) 2016 Bogdan Gureev

//------------------------------------------------------------------------------

Extractors = {};
(function(){

//------------------------------------------------------------------------------

	function getIntValue( _value )
	{
		return parseInt( _value );
	}

//------------------------------------------------------------------------------

	function getDoubleValue( _value )
	{
		return parseFloat( _value );
	}

//------------------------------------------------------------------------------

	function getSafeNumberValue( _valueExtractor, _value )
	{
		if ( !_value )
			return 0;

		var safeValue = _valueExtractor( _value );
		if ( !safeValue || isNaN( safeValue ) )
			return 0;

		return safeValue;
	}

//------------------------------------------------------------------------------

	function getSafeIntValue( _value )
	{
		return getSafeNumberValue( getIntValue, _value );
	}

//------------------------------------------------------------------------------

	function getSafeDoubleValue( _value )
	{
		return getSafeNumberValue( getDoubleValue, _value );
	}

//------------------------------------------------------------------------------

	this.getIntValue = getIntValue;
	this.getSafeIntValue = getSafeIntValue;
	this.getDoubleValue = getDoubleValue;
	this.getSafeDoubleValue = getSafeDoubleValue;

//------------------------------------------------------------------------------

}).apply( Extractors );

//------------------------------------------------------------------------------

Extractors.Array = {};
(function(){

//------------------------------------------------------------------------------

	function getItemsCount( _itemsList )
	{
		if ( !_itemsList )
			throw "getItemsCount: !_itemsList";

		return _itemsList.length;
	}

//------------------------------------------------------------------------------

	function getSpecificItemsCount( _itemsList, _handler )
	{
		var specificItemsCount = 0;

		forEachItem(
				_itemsList
			,	function ( _value )
				{
					if ( _handler( _value ) )
						++specificItemsCount;
				}
		);

		return specificItemsCount;
	}

//------------------------------------------------------------------------------

	function forEachItem( _itemsList, _handler )
	{
		var itemsCount = getItemsCount( _itemsList );
		for ( var i = 0; i < itemsCount; ++i )
		{
			var item = _itemsList[ i ];
			if ( item.length != 1 )
				throw "forEachItem: item.length != 1";

			_handler( item[ 0 ] );
		}
	}

//------------------------------------------------------------------------------

	function getStudentsCount( _extractor, _groupsList )
	{
		var groupsCount = _groupsList.length;

		var all = 0;
		var selected = 0;
		for ( var i = 0; i < groupsCount; ++i )
		{
			var groupData = _groupsList[ i ];

			if ( Helpers.isEmpty( groupData ) )
				continue;

			all += getItemsCount( groupData );
			selected += _extractor( groupData );
		}

		var ratio = Helpers.getPercentagesValue( selected, all );
		return {
				all: all
			,	selected: selected
			,	ratio: ratio
		};
	}

//------------------------------------------------------------------------------

	this.getSpecificItemsCount = getSpecificItemsCount;
	this.forEachItem = forEachItem;

	this.getStudentsCount = getStudentsCount;

//------------------------------------------------------------------------------

}).apply( Extractors.Array );

//------------------------------------------------------------------------------

Extractors.Range = {};
(function(){

//------------------------------------------------------------------------------

	function getItemsCount( _itemsList )
	{
		if ( !_itemsList )
			throw "getItemsCount: !_itemsList";

		return _itemsList.length;
	}

//------------------------------------------------------------------------------

	function getRangeItemsCount( _range )
	{
		if ( !_range )
			throw "getRangeItemsCount: !_range";

		return _range.getNumRows();
	}

//------------------------------------------------------------------------------

	function getSpecificItemsCount( _itemsList, _handler )
	{
		var specificItemsCount = 0;

		forEachItem(
				_itemsList
			,	function ( _value )
				{
					if ( _handler( _value ) )
						++specificItemsCount;
				}
		);

		return specificItemsCount;
	}

//------------------------------------------------------------------------------

	function forEachItem( _itemsList, _handler )
	{
		var itemsCount = getItemsCount( _itemsList );
		for ( var i = 0; i < itemsCount; ++i )
		{
			var item = _itemsList[ i ];
			if ( item.length != 1 )
				throw "forEachItem: item.length != 1";

			_handler( item[ 0 ] );
		}
	}

//------------------------------------------------------------------------------

	function getRange( _textRange )
	{
		var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
		return spreadsheet.getRange( _textRange );
	}

//------------------------------------------------------------------------------

	function getStudentsCount( _extractor, _groupsList )
	{
		var groupsCount = _groupsList.length;

		var all = 0;
		var selected = 0;
		for ( var i = 0; i < groupsCount; ++i )
		{
			var groupLink = _groupsList[ i ];

			if ( Helpers.isEmpty( groupLink ) )
				continue;

			var range = getRange( groupLink );
			all += getRangeItemsCount( range );
			selected += _extractor( range );
		}

		var ratio = Helpers.getPercentagesValue( selected, all );
		return {
				all: all
			,	selected: selected
			,	ratio: ratio
		};
	}

//------------------------------------------------------------------------------

	this.getRangeItemsCount = getRangeItemsCount;
	this.getSpecificItemsCount = getSpecificItemsCount;
	this.forEachItem = forEachItem;
	this.getRange = getRange;

	this.getStudentsCount = getStudentsCount;

//------------------------------------------------------------------------------

}).apply( Extractors.Range );

//------------------------------------------------------------------------------
