// (C) 2016 Bogdan Gureev

//------------------------------------------------------------------------------

Debug = {};
(function(){

//------------------------------------------------------------------------------

	function getBackgroundColor( _textRange )
	{
		return Extractors.Range.getRange( _textRange ).getBackgrounds();
	}

//------------------------------------------------------------------------------

	function getTextColor( _textRange )
	{
		return Extractors.Range.getRange( _textRange ).getFontColors();
	}

//------------------------------------------------------------------------------

	this.getBackgroundColor = getBackgroundColor;
	this.getTextColor = getTextColor;

//------------------------------------------------------------------------------

}).apply( Debug );

//------------------------------------------------------------------------------
