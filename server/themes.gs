// (C) 2016 Bogdan Gureev

//------------------------------------------------------------------------------

Themes = {};
(function(){

//------------------------------------------------------------------------------

	function getFreeThemesCount( _textRange )
	{
		var range = Extractors.Range.getRange( _textRange );
		return Themes.Handlers.getFreeThemesCount( range );
	}

//------------------------------------------------------------------------------

	function getSentThemesCount( _textRange )
	{
		var range = Extractors.Range.getRange( _textRange );

		var busyThemesCount = Themes.Handlers.getBusyThemesCount( range );
		var sentThemesCount = Themes.Handlers.getSentThemesCount( range );

		return sentThemesCount + " / " + busyThemesCount;
	}

//------------------------------------------------------------------------------

	function getStudentsWithThemesCount( _groupsList )
	{
		var summary = Extractors.Range.getStudentsCount(
				Themes.Handlers.getStudentsWithThemesCount
			,	_groupsList
		);
		return summary.selected + " / " + summary.all;
	}

//------------------------------------------------------------------------------

	function getThemesPerformance( _groupsList )
	{
		var groupsCount = _groupsList.length;

		var result = [];
		var totalPerformance = [];
		for ( var i = 0; i < groupsCount; ++i )
		{
			var groupData = _groupsList[ i ];

			if ( Helpers.isEmpty( groupData ) )
				continue;

			var localPerformance = Themes.Handlers.getThemesPerformance( groupData );
			for ( var theme in localPerformance )
			{
				if ( totalPerformance[ theme ] === undefined )
					totalPerformance[ theme ] = [];

				Helpers.assignArrayValue(
						totalPerformance[ theme ]
					,	localPerformance[ theme ]
				);
			}
		}

		for ( var theme in totalPerformance )
		{
			var info = [];
			info.push( theme );

			var marks = totalPerformance[ theme ];

			var themeMark = 0;
			var themeStudents = marks.length;

			if ( themeStudents == 0 )
				themeMark = 0;
			else
			{
				for ( var i = 0; i < themeStudents; ++i )
					themeMark += Extractors.getSafeIntValue( marks[ i ] );

				themeMark = themeMark / themeStudents;
				themeMark = themeMark.toFixed( 2 );
			}

			info.push( themeMark );
			info.push( themeStudents );

			result.push( info );
		}

		result.sort(
			function ( _left, _right )
			{
				var left = Extractors.getSafeDoubleValue( _left[ 1 ] );
				var right = Extractors.getSafeDoubleValue( _right[ 1 ] );

				if ( left === right )
					return 0;
				else
					return ( left < right ) ? 1 : -1;
			}
		);

		return result;
	}

//------------------------------------------------------------------------------

	this.getFreeThemesCount = getFreeThemesCount;
	this.getSentThemesCount = getSentThemesCount;

	this.getStudentsWithThemesCount = getStudentsWithThemesCount;

	this.getThemesPerformance = getThemesPerformance;

//------------------------------------------------------------------------------

}).apply( Themes );

//------------------------------------------------------------------------------

Themes.Handlers = {};
(function(){

//------------------------------------------------------------------------------

	function getBusyThemesCount( _range )
	{
		var fontLines = _range.getFontLines();

		return Extractors.Range.getSpecificItemsCount(
				fontLines
			,	function ( _value )
				{
					return _value == Resources.Configuration.StyleLineThrough;
				}
		);
	}

//------------------------------------------------------------------------------

	function getFreeThemesCount( _range )
	{
		var busyThemesCount = getBusyThemesCount( _range );
		var allThemesCount = Extractors.Range.getRangeItemsCount( _range );

		return allThemesCount - busyThemesCount;
	}

//------------------------------------------------------------------------------

	function getSentThemesCount( _range )
	{
		var backgrounds = _range.getBackgrounds();

		return Extractors.Range.getSpecificItemsCount(
				backgrounds
			,	function ( _value )
				{
					return _value != Resources.Configuration.ColorWhite;
				}
		);
	}

//------------------------------------------------------------------------------

	function getStudentsWithThemesCount( _range )
	{
		var fontStyles = _range.getFontStyles();

		return Extractors.Range.getSpecificItemsCount(
				fontStyles
			,	function ( _value )
				{
					return _value != Resources.Configuration.StyleItalic;
				}
		);
	}

//------------------------------------------------------------------------------

	function getThemesPerformance( _groupData )
	{
		var themeIndex = 0;
		var markIndex = _groupData.length / 2;

		var performance = [];

		for ( var i = 0; i < markIndex; ++i )
		{
			var theme = _groupData[ themeIndex + i ];
			var mark = _groupData[ markIndex + i ];

			if ( performance[ theme ] === undefined )
				performance[ theme ] = [];

			Helpers.assignIntValue( performance[ theme ], mark );
		}

		return performance;
	}

//------------------------------------------------------------------------------

	this.getBusyThemesCount = getBusyThemesCount;
	this.getFreeThemesCount = getFreeThemesCount;
	this.getSentThemesCount = getSentThemesCount;

	this.getStudentsWithThemesCount = getStudentsWithThemesCount;

	this.getThemesPerformance = getThemesPerformance;

//------------------------------------------------------------------------------

}).apply( Themes.Handlers );

//------------------------------------------------------------------------------
